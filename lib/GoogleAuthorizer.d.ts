import { OAuth2Client } from 'google-auth-library';
export declare const authorizeAccount: (credentialsObj: string, tokenObj: string) => Promise<OAuth2Client>;
