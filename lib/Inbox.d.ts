import { gmail_v1 } from 'googleapis';
import { InboxMethods } from './InboxMethods.interface';
import { Label } from './Label.interface';
import { SearchQuery } from './SearchQuery.interface';
export interface Message {
    messageId: string;
    threadId: string;
    subject: string | undefined;
    from: string | undefined;
    to: string | undefined;
    receivedOn: string | undefined;
    labelIds: string[];
    snippet: string;
    historyId: string;
    /**
     * unix ms timestamp string
     */
    internalDate: string;
    getFullMessage: () => any;
    body: {
        html: string | undefined;
        text: string | undefined;
    };
}
export declare class Inbox implements InboxMethods {
    private credentialsObj;
    private tokenObj;
    private gmailApi;
    private authenticated;
    constructor(credentialsObj: any, tokenObj: any);
    authenticateAccount(): Promise<void>;
    getAttachment(userId: string, messageId: string, id: string): Promise<import("gaxios").GaxiosResponse<gmail_v1.Schema$MessagePartBody>>;
    getAllLabels(): Promise<Label[]>;
    /**
     * Retrieves all existing emails
     */
    getLatestMessages(): Promise<Message[]>;
    /**
     * Finds existing emails
     *
     * Example search query
     * - "has:attachment filename:salary.pdf largerThan:1000000 label:(paychecks salaries) from:myoldcompany@oldcompany.com"
     * - {
     *   has: "attachment",
     *   filename: "salary.pdf",
     *   largerThanInBytes: 1000000,
     *   labels: ["paychecks", "salaries"],
     *   from: "myoldcompany@oldcompany.com"
     * }
     */
    findMessages(searchQuery: SearchQuery | string | undefined): Promise<Message[]>;
    /**
     *
     * @param searchQuery similar to findMessages, the query how it will find the message
     * @param timeTillNextCallInSeconds How long it should wait till it checks again if the message is received
     * @param maxWaitTimeInSeconds How long it should wait in total for the message
     */
    waitTillMessage(searchQuery: SearchQuery | string | undefined, shouldLogEvents?: boolean, timeTillNextCallInSeconds?: number, maxWaitTimeInSeconds?: number): Promise<Message[]>;
    private log;
    private timeout;
    private getMessageById;
    private guardAuthentication;
    private arrayToAdvancedSearchString;
    private mapSearchQueryToSearchString;
    private mapDateTypeToQuery;
    private formatDate;
}
