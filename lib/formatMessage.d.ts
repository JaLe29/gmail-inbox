import { gmail_v1 } from 'googleapis';
import { Message } from './Inbox';
/**
 * Method is being called by Inbox class
 *
 * @param message
 */
export declare const formatMessage: (message: {
    data: gmail_v1.Schema$Message;
}) => Message;
