"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Method is being called by Inbox class
 *
 * @param message
 */
exports.formatMessage = function (message) {
    var _a;
    var getReceivedOn = function () {
        var rHeader = getHeader('Received', headers);
        if (!rHeader)
            return 'error';
        var parts = rHeader.split(";");
        return parts[parts.length - 1].trim();
    };
    var headers = (_a = message.data.payload) === null || _a === void 0 ? void 0 : _a.headers;
    var prettyMessage = {
        body: getMessageBody(message),
        from: getHeader('From', headers),
        historyId: message.data.historyId,
        internalDate: message.data.internalDate,
        labelIds: message.data.labelIds,
        messageId: message.data.id,
        snippet: message.data.snippet,
        threadId: message.data.threadId,
        to: getHeader('To', headers),
        subject: getHeader('Subject', headers),
        receivedOn: getReceivedOn(),
        getFullMessage: function () { return message.data.payload; },
    };
    return prettyMessage;
};
var getHeader = function (name, headers) {
    if (!headers) {
        return;
    }
    var header = headers.find(function (h) { return h.name === name; });
    return header && header.value;
};
var getMessageBody = function (message) {
    var _a, _b, _c;
    var body = {};
    var messagePayload = message.data.payload;
    var messageBody = (_a = messagePayload) === null || _a === void 0 ? void 0 : _a.body;
    if (((_b = messageBody) === null || _b === void 0 ? void 0 : _b.size) && messagePayload) {
        switch ((_c = messagePayload) === null || _c === void 0 ? void 0 : _c.mimeType) {
            case 'text/html':
                body.html = Buffer.from(messageBody.data, 'base64').toString('utf8');
                break;
            case 'text/plain':
            default:
                body.text = Buffer.from(messageBody.data, 'base64').toString('utf8');
                break;
        }
    }
    else {
        body = getPayloadParts(message);
    }
    return body;
};
var getPayloadParts = function (message) {
    var _a, _b, _c, _d;
    var body = {};
    var parts = (_a = message.data.payload) === null || _a === void 0 ? void 0 : _a.parts;
    var hasSubParts = (_b = parts) === null || _b === void 0 ? void 0 : _b.find(function (part) { var _a; return (_a = part.mimeType) === null || _a === void 0 ? void 0 : _a.startsWith('multipart/'); });
    if (hasSubParts) {
        // recursively continue until you find the content
        var newMessage = {
            Headers: {},
            config: {},
            data: { payload: hasSubParts },
        };
        return getPayloadParts(newMessage);
    }
    var htmlBodyPart = (_c = parts) === null || _c === void 0 ? void 0 : _c.find(function (part) { return part.mimeType === 'text/html'; });
    if (htmlBodyPart && htmlBodyPart.body && htmlBodyPart.body.data) {
        body.html = Buffer.from(htmlBodyPart.body.data, 'base64').toString('utf8');
    }
    var textBodyPart = (_d = parts) === null || _d === void 0 ? void 0 : _d.find(function (part) { return part.mimeType === 'text/plain'; });
    if (textBodyPart && textBodyPart.body && textBodyPart.body.data) {
        body.text = Buffer.from(textBodyPart.body.data, 'base64').toString('utf8');
    }
    return body;
};
